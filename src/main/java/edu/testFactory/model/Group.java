package edu.testFactory.model;

import edu.testFactory.dao.Identifier;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Created by UserSts on 26.04.2017.
 */
public class Group implements Identifier<Integer> {

    private final IntegerProperty id;
    private final StringProperty name;

    public Group() {
        this.id = new SimpleIntegerProperty();
        this.name = new SimpleStringProperty();
    }

    @Override
    public Integer getId() {
        return null;
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }
}
