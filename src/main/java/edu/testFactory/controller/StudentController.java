package edu.testFactory.controller;

import edu.testFactory.dao.MenuInterface;
import edu.testFactory.model.Student;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class StudentController implements Initializable, MenuInterface<Student>{

    @FXML private TableView<Student> tableStudents;
    @FXML private TableColumn<Student, String> tableStudentsColumnName;
    @FXML private TableColumn<Student, String> tableStudentsColumnSurname;
    @FXML  private TextField textFieldName;
    @FXML private TextField textFieldSurname;

    //private ObservableList<Student> personData =

    public StudentController() {
    }

    public void initialize(URL location, ResourceBundle resources) {
        tableStudentsColumnName.setCellValueFactory(callData -> callData.getValue().nameProperty());
        tableStudentsColumnSurname.setCellValueFactory(cellData -> cellData.getValue().surnameProperty());
    }

    @Override
    public void populateObjects(ObservableList<Student> objData) {

    }

    @Override
    public ObservableList<Student> getObservableListData() {
        return null;
    }

    @Override
    public void searchObjects() {

    }
}
