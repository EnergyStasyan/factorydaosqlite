package edu.testFactory.controller;

import edu.testFactory.dao.AbstractTController;
import edu.testFactory.model.Student;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by UserSts on 27.04.2017.
 */
public class testStudentController extends AbstractTController<Student> implements Initializable {

    @FXML private TableColumn<Student, String> studentsColumnName;
    @FXML private TableColumn<Student, String> studentsColumnSurname;
    @FXML private TableColumn<Student, Integer> studentsColumnGroup;
    @FXML  private TextField textFieldName;
    @FXML private TextField textFieldSurname;


    public testStudentController() {
        super(Student.class.getName());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        studentsColumnName.setCellValueFactory(callData -> callData.getValue().nameProperty());
        studentsColumnSurname.setCellValueFactory(cellData -> cellData.getValue().surnameProperty());
        studentsColumnGroup.setCellValueFactory(cellData -> cellData.getValue().group_idProperty().asObject());
        searchObjects();
    }
}
