package edu.testFactory.dao;

import edu.testFactory.model.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by UserSts on 26.04.2017.
 */
public class SqLiteStudentDao extends AbstractJDBCDao<Student, Integer> {

    public SqLiteStudentDao(Connection connection) {
        super(connection);
    }

    @Override
    public Student create() throws PersistException {
       Student s = new Student();
       return persist(s);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM students";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO students ( name, surname, group_id) VALUES(?,?,?)";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE students SET name = ?, surname = ?, group_id = ? WHERE id = ?";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE students WHERE id = ?";
    }

    @Override
    protected ObservableList<Student> parseResultSet(ResultSet rs) throws PersistException {
        ObservableList<Student> result = FXCollections.observableArrayList();
        try{
            while (rs.next()){
                Student s = new Student();
                s.setId(rs.getInt("id"));
                s.setName(rs.getString("name"));
                s.setSurname(rs.getString("surname"));
                s.setGroup_id(rs.getInt("group_id"));
                result.add(s);
            }
        }catch (Exception e){
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Student object) throws PersistException {
//        Date sqlDate = convert(object.)
        try {
            ps.setString(1, object.getName());
            ps.setString(2, object.getSurname());
            ps.setInt(3, object.getGroup_id());

        } catch (SQLException e) {
           throw new PersistException(e);
        }

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Student object) throws PersistException { // FIXME: 01.05.2017 закончить

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Student object) throws PersistException {

    }

    protected java.sql.Date convert(java.util.Date date){
        if (date == null) {
            return null;
        }
        return new java.sql.Date(date.getTime());
    }
 }
