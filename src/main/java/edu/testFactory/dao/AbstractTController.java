package edu.testFactory.dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;

import java.sql.Connection;

/**
 * Created by UserSts on 27.04.2017.
 */
public abstract class AbstractTController<E> {
    @FXML private TableView<E> objTable;
    private ObservableList<E> objData = FXCollections.observableArrayList();
    private String dClass;

    public AbstractTController(String dClass) {
        this.dClass = dClass;
    }

    private void populateObjects(ObservableList<E> objData) {
        objTable.setItems(objData);
    }


    public ObservableList<E> getObservableListData() {
        return objData;
    }


    public void searchObjects()  {


        DaoFactory<Connection> factory = new SqLiteDaoFactory();
        try {
            Connection connection = factory.getContext();
            GenericDao<E, ?> dao = factory.getDao(connection, Class.forName(dClass));
//            GenericDao<E, ?> dao = factory.getDao(connection, Student.class);

            ObservableList<E> objData = dao.getAll();
            // FIXME: 02.05.2017 Насколько это расточительно для памяти и не лучше ли  populateObjects(dao.getAll())
           populateObjects(objData);

        }catch (PersistException |ClassNotFoundException e){
            e.printStackTrace();
        }
    }


}
