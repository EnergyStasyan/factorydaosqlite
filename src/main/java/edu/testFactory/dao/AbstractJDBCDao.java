package edu.testFactory.dao;

import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * не понимаю зачем нужны Identifier<PK>
 */
public abstract class AbstractJDBCDao <T extends Identifier<PK>, PK extends Integer> implements GenericDao<T,PK >{

    private Connection connection;

    /**
     * Возвращает sql запрос для получения всех записей.
     * <p/>
     * SELECT * FROM [Table]
     */
    public abstract String getSelectQuery();

    /**
     * Возвращает sql запрос для вставки новой записи в базу данных.
     * <p/>
     * INSERT INTO [Table] ([column, column, ...]) VALUES (?, ?, ...);
     */
    public abstract String getCreateQuery();

    /**
     * Возвращает sql запрос для обновления записи.
     * <p/>
     * UPDATE [Table] SET [column = ?, column = ?, ...] WHERE id = ?;
     */
    public abstract String getUpdateQuery();

    /**
     * Возвращает sql запрос для удаления записи из базы данных.
     * <p/>
     * DELETE FROM [Table] WHERE id= ?;
     */
    public abstract String getDeleteQuery();



    protected abstract ObservableList<T> parseResultSet(ResultSet rs) throws PersistException;

    protected abstract void prepareStatementForInsert(PreparedStatement ps, T object) throws PersistException;
    protected abstract void prepareStatementForUpdate(PreparedStatement ps, T object) throws PersistException;
    protected abstract void prepareStatementForDelete(PreparedStatement ps, T object) throws PersistException;

    public AbstractJDBCDao(Connection connection) {
        this.connection = connection;
    }

    @Override
    public T create() throws PersistException {
        return null;
    }

    @Override
    public T persist(T object) throws PersistException {
        if (object.getId() != null){
            throw new PersistException("Object is already persist");
        }

        T persistInstance;
        String sql = getCreateQuery();

        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            prepareStatementForInsert(ps,object);
            int count = ps.executeUpdate();
            if (count > 1) {
                throw new PersistException("On persist modify more then 1 record: " + count);
            }
        }catch (Exception e){
            throw new PersistException( e);
        }

        sql = getSelectQuery() + " WHERE id = last_insert_rowid()";

        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ResultSet rs = ps.executeQuery();
            ObservableList<T> observableList = parseResultSet(rs);   // TODO: 26.04.2017  возможно лучше сменить observableList -> List
            if (observableList == null|| observableList.size() != 1){
                throw new PersistException("Exception on findByPk new persist data");
            }
            persistInstance = observableList.iterator().next();
        }catch (Exception e){
            throw new PersistException(e);
        }
        return persistInstance;
    }

    @Override
    public T getByPk(int key) throws PersistException {
        ObservableList<T> observableList;       // TODO: 26.04.2017  возможно лучше сменить observableList -> List
        String sql = getSelectQuery();
        sql +=" WHERE id = ?";
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, key);
            ResultSet rs = ps.executeQuery();
            observableList = parseResultSet(rs);
        }catch (Exception e){
            throw new PersistException(e);
        }
        if (observableList == null || observableList.size() == 0){
            return null;
        }
        if (observableList.size() > 1){
            throw new PersistException("Received more than one record");
        }
        return observableList.iterator().next();
    }

    @Override
    public void update(T object) throws PersistException {
        String sql = getUpdateQuery();
        try(PreparedStatement ps = connection.prepareStatement(sql)) {
            prepareStatementForUpdate(ps, object);
            int count = ps.executeUpdate();
            if (count != 1){
                throw new PersistException("On update modify more then 1 record" + count);
            }
        }catch (Exception e){
            throw new PersistException(e);
        }
    }

    @Override
    public void delete(T object) throws PersistException {
        String sql = getDeleteQuery();
        try(PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setObject(1, object.getId());
            int count = ps.executeUpdate();
            if (count != 1){
                throw new PersistException("on delete modify more then 1 record" + count);
            }
        }catch (Exception e){
            throw new PersistException(e);
        }

    }

    @Override
    public ObservableList<T> getAll() throws PersistException {
       ObservableList<T> observableList;
       String sql = getSelectQuery();
       try(PreparedStatement ps = connection.prepareStatement(sql)) {
           ResultSet rs = ps.executeQuery();
           observableList = parseResultSet(rs);
       }catch (Exception e){
           throw new PersistException(e);
       }
        return observableList;
    }
}
