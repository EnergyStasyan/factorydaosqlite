package edu.testFactory.dao;

/**
 * Created by UserSts on 26.04.2017.
 */
public interface DaoFactory<C> {

    interface DaoCreator<C>{
         GenericDao create(C context);
    }

    C getContext() throws PersistException;
    GenericDao getDao(C context, Class dtoClass) throws PersistException;


}
