package edu.testFactory.dao;

import edu.testFactory.model.Group;
import edu.testFactory.model.Student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by UserSts on 26.04.2017.
 */
public class SqLiteDaoFactory implements DaoFactory<Connection>{

    private String JDBC_DRIVER = "org.sqlite.JDBC";
    private String connStr = "jdbc:sqlite:testFactory.db";
    private Map<Class, DaoCreator> creators;


    public SqLiteDaoFactory() {
        try{
            Class.forName(JDBC_DRIVER);
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }

        creators = new HashMap<Class, DaoCreator>();
        creators.put(Student.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqLiteStudentDao(connection);
            }
        });

        creators.put(Group.class, (DaoCreator<Connection>) SqLiteStudentDao::new);
    }

    @Override
    public Connection getContext() throws PersistException {
       Connection connection;
       try {
           connection = DriverManager.getConnection(connStr);
       }catch (SQLException e){
           throw new PersistException(e);
       }
        return connection;
    }

    @Override
    public GenericDao getDao(Connection context, Class dtoClass) throws PersistException {
        DaoCreator creator = creators.get(dtoClass);
        if (creator == null) {
            throw new  PersistException("Dao object for " + dtoClass + " not found");
        }

        return creator.create(context);
    }
}
