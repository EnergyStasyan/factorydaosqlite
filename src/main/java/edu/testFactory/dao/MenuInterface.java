package edu.testFactory.dao;

import javafx.collections.ObservableList;

/**
 * Created by UserSts on 27.04.2017.
 */
public interface MenuInterface<E> {

    /**
     * в таблицу <E>sTable устанавливаем элементы  из массива <E>ObservableList
     * @param objData
     */
    void populateObjects(ObservableList<E> objData);

    /**
     *  возвращает
     * @return ObservableList<E>
     */
    ObservableList<E> getObservableListData();

    /**
     * поиск объектов и вывод их на экран
     */
    void searchObjects();
}
