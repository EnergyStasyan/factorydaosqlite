package edu.testFactory.dao;

import java.io.Serializable;

/**
 * Created by UserSts on 26.04.2017.
 */
public interface Identifier<PK extends Serializable> {
    PK getId();
}
