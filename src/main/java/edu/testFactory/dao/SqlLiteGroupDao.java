package edu.testFactory.dao;

import edu.testFactory.model.Group;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by UserSts on 26.04.2017.
 */
public class SqlLiteGroupDao extends AbstractJDBCDao<Group, Integer>  {

    public SqlLiteGroupDao(Connection connection) {
        super(connection);
    }

    @Override
    public String getSelectQuery() {
        return null;
    }

    @Override
    public String getCreateQuery() {
        return null;
    }

    @Override
    public String getUpdateQuery() {
        return null;
    }

    @Override
    public String getDeleteQuery() {
        return null;
    }

    @Override
    protected ObservableList<Group> parseResultSet(ResultSet rs) throws PersistException {
        return null;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement ps, Group object) throws PersistException {

    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement ps, Group object) throws PersistException {

    }

    @Override
    protected void prepareStatementForDelete(PreparedStatement ps, Group object) throws PersistException {

    }
}
