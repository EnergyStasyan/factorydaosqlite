package edu.testFactory.dao;

import javafx.collections.ObservableList;

import java.io.Serializable;

/**
 * Created by UserSts on 26.04.2017.
 */
public interface GenericDao<T, PK extends Serializable> {

    T create() throws PersistException;
    T persist(T object) throws PersistException;
    T getByPk(int key) throws PersistException;
    void update(T object) throws PersistException;
    void delete(T object) throws PersistException;
    ObservableList<T> getAll() throws PersistException;

}
