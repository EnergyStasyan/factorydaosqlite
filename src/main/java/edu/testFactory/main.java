package edu.testFactory;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by UserSts on 25.04.2017.
 */
public class main extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;
    private AnchorPane tableStudentsLayout;


    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Test Factory");
        initRootLayout();
        tableStudents();

    }

    public void initRootLayout(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("rootLayout.fxml"));
            rootLayout = loader.load();
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void tableStudents(){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("studentsLayout.fxml"));
            tableStudentsLayout = loader.load();
            rootLayout.setLeft(tableStudentsLayout);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
